/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tcaron <tcaron@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/09 16:26:51 by tcaron            #+#    #+#             */
/*   Updated: 2014/02/18 15:11:34 by erobert          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <signal.h>
#include <stdlib.h>
#include "../header.h"
#include "../libft.h"
/*
static void		sigint_handler(int sig)
{
	if (sig == 2)
		return ;
	else if (sig == 3)
		exit(0);
	return ;
}
*/
void			ft_signal(void)
{
//	signal(SIGINT, sigint_handler);
//	signal(SIGQUIT, SIG_IGN);
}
